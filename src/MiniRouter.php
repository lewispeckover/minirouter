<?php
class MiniRouter {
	private $routes = Array();
	private $router = null;

	public function register() {
		$this->routes[] = func_get_args();
	}

	public function init() {
		$this->router = \FastRoute\simpleDispatcher(function(\FastRoute\RouteCollector $r) {
			foreach ($this->routes as $route) {
				$r->addRoute($route[0], $route[1], $route[2]);
			}
		});
	}

	public function go($method, $path) {
		if ($this->router === null) $this->init();
		$state = $this->router->dispatch($_SERVER['REQUEST_METHOD'], $_SERVER['REDIRECT_URL']);
		switch ($state[0]) {
			case \FastRoute\Dispatcher::NOT_FOUND:
				$this->respond(404, ["message"=>"Path not found"]);
				break;
			case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
				$allowedMethods = $state[1];
				$this->respond(405, ['message'=>'Invalid method. Valid methods are: '.join($state[1], ',')]);
				break;
			case \FastRoute\Dispatcher::FOUND:
				$handler = $state[1];
				$args = empty($state[2])? [] : $state[2];
				$response = @call_user_func_array($handler, $args);
				$response = empty($response)? [false] : $response;
				call_user_func_array('MiniRouter::respond', $response);
				break;
		}
	}

	public static function respond($status, $body='') {
		if ($status === false)
			MiniRouter::respond(500, ['message'=>'An internal error occurred.']);
		if (is_array($status)) {
			http_response_code(array_pop($status));
			while($h = array_pop($status)) {
				header($h);
			}
		}
		else {
			http_response_code($status);
		}
		echo json_encode($body);
		exit();
	}
}
